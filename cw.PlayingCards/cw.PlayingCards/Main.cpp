#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Suit
{
	CLUBS, DIAMONDS, HEARTS, SPADES
};

enum Rank
{
	ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING
};

struct Card
{
	Suit suit;
	Rank rank;
};

int main()
{
	_getch();
	return 0;
}